<?php
/**
 * Created by PhpStorm.
 * User: lv
 * Date: 08.06.18
 * Time: 11:03
 */

namespace ThiocynCustomerGroups\Repositories;


use Shopware\Components\Model\ModelManager;
use Shopware\Models\Customer\Customer;
use Shopware\Models\Customer\Group;
use Shopware\Models\Order\Order;

class UserRepository
{
    private $modelManager;

    public function __construct(ModelManager $modelManager)
    {
        $this->modelManager = $modelManager;
    }

    public function getUserById($userId)
    {
        $criteria = ['id' => $userId];

        return $this->modelManager->getRepository(Customer::class)->findOneBy($criteria);
    }

    public function countOrderByUserId($userId)
    {
        $criteria = ['customer' => $userId];
        return count($this->modelManager->getRepository(Order::class)->findBy($criteria));
    }

    /**
     * @param $userId
     * @param $customerGroup
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateCustomerGroup($userId, $customerGroup)
    {
        /** @var Customer $customer */
        $customer = $this->getUserById($userId);
        $customer->setGroup($this->getGroupByKey($customerGroup));

        $this->modelManager->persist($customer);
        $this->modelManager->flush();
    }

    /**
     * @param $groupkey
     * @return bool|Group
     */
    public function getGroupByKey($groupkey)
    {
        $criteria = ['key' => $groupkey];

        if(!$group = $this->modelManager->getRepository(Group::class)->findOneBy($criteria)){
            print_r("GruppenKey $groupkey nicht gefunden");
            return false;
        } else {
            return $group;
        }
    }
}