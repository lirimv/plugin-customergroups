<?php
/**
 * Created by PhpStorm.
 * User: lv
 * Date: 08.06.18
 * Time: 10:01
 */

namespace ThiocynCustomerGroups\Subscriber;


use Enlight\Event\SubscriberInterface;
use ThiocynCustomerGroups\Service\CustomerGroupUpdater;

class FrontendCheckoutFinishSubscriber implements SubscriberInterface
{
    /** @var CustomerGroupUpdater  */
    private $customerGroupUpdater;

    public function __construct(CustomerGroupUpdater $customerGroupUpdater)
    {
        $this->customerGroupUpdater = $customerGroupUpdater;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return ['Shopware_Controllers_Frontend_Checkout::finishAction::after' => 'onCheckoutFinish'];
    }

    /**
     * @param \Enlight_Hook_HookArgs $args
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onCheckoutFinish(\Enlight_Hook_HookArgs $args)
    {
        /** @var \Shopware_Controllers_Frontend_Checkout $subject */
        $subject = $args->getSubject();
        $userData = $subject->getUserData();
        $customerId = $userData['additional']['user']['userID'];

        $this->customerGroupUpdater->updateCustomerGroup($customerId);
    }
}