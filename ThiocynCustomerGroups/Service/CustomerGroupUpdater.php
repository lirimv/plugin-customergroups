<?php
/**
 * Created by PhpStorm.
 * User: lv
 * Date: 08.06.18
 * Time: 10:53
 */

namespace ThiocynCustomerGroups\Service;

use Shopware\Components\Logger;
use Shopware\Components\Plugin\ConfigReader;
use Shopware\Models\Customer\Customer;
use ThiocynCustomerGroups\Repositories\UserRepository;
use ThiocynCustomerGroups\ThiocynCustomerGroups;

class CustomerGroupUpdater
{
    /** @var UserRepository  */
    private $userRepoistory;

    /** @var Logger  */
    private $logger;

    /** @var ConfigReader */
    private $configReader;

    /** @var string */
    private $pluginName;

    /**
     * CustomerGroupUpdater constructor.
     * @param UserRepository $userRepository
     * @param Logger $logger
     * @param ConfigReader $configReader
     * @param $pluginName
     */
    public function __construct(UserRepository $userRepository, Logger $logger, ConfigReader $configReader, $pluginName)
    {
        $this->userRepoistory = $userRepository;
        $this->logger = $logger;
        $this->configReader = $configReader;
        $this->pluginName = $pluginName;
    }

    /**
     * @param $userId
     * @return bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateCustomerGroup($userId)
    {
        $pluginConfig = $this->configReader->getByPluginName($this->pluginName);

        if($pluginConfig['exceptions']) {
            $exceptionList = explode('|', $pluginConfig['exceptions']);
        }

        if(empty($userId))
        {
            $this->logger->log('error',"UserId ist leer");
            return false;
        }

        /** @var Customer $user */
        if(!$user = $this->userRepoistory->getUserById($userId))
        {
            $this->logger->log('error', "User mit ID $userId nicht gefunden");
            return false;
        }

        if(in_array($user->getGroup()->getKey(), $exceptionList )){
            $this->logger->log('info', "User: $userId in Ausnahmeliste für Kundengruppen", $exceptionList);
            return false;
        }

        $orderAmount = $this->userRepoistory->countOrderByUserId($userId);

        if($orderAmount == 1){

            if(ThiocynCustomerGroups::CUSTOMER_GROUP_EB_KEY == $user->getGroup()->getKey()){
                return true;
            }

            $this->userRepoistory->updateCustomerGroup($userId, ThiocynCustomerGroups::CUSTOMER_GROUP_EB_KEY);
            $this->logger->log('info', "User: $userId wurde der Gruppe Erstbesteller hinzugefügt");

        } else {
            if(ThiocynCustomerGroups::CUSTOMER_GROUP_EK_KEY == $user->getGroup()->getKey()){
                return true;
            }

            $this->userRepoistory->updateCustomerGroup($userId, ThiocynCustomerGroups::CUSTOMER_GROUP_EK_KEY);
            $this->logger->log('info', "User: $userId wurde der Gruppe Shopkunden hinzugefügt");
        }

        return true;
    }
}