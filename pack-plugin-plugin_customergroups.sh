#!/usr/bin/env bash

BRANCH_NAME=$(git rev-parse --abbrev-ref HEAD)
NORMALIZED_BRANCH_NAME="${BRANCH_NAME//\//_}"

zip -r plugin-customergroups-${NORMALIZED_BRANCH_NAME}.zip . -x ".*" "*.sh" "*.zip" "README.md" "./data/*"
